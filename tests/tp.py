# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    result = "0"
    if n == 0:
        return result
    result = "S" * n + result
    return result


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    b = str(b)
    if str(a).startswith("S"):
        return S(addition(a[1:],b))



def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"
    if b == "0":
        return "0"
    return addition(multiplication(a[1:], b), b) 


def facto_ite(n: int) -> int:
    if n == 0 or n == 1:
        return 1
    result = 1
    for x in range(1, n+1):
        result *= x
    return result



def facto_rec(n: int) -> int:
    if n == 0 or n == 1:
        return 1
    return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo_rec(n - 1) + fibo_rec(n - 2)
    

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    anc = 1
    tanc = 0
    for x in range(1, n):
        actuel = anc + tanc
        tanc = anc
        anc = actuel
    return actuel

def golden_phi(n: int) -> int:
    return fibo_ite(n + 1) / fibo_ite(n)
    


def sqrt5(n: int) -> int:
    return golden_phi(n) * 2 - 1


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n == 1:
        return a
    elif n % 2 == 0:
        temp = pow(a, n // 2)
        return temp * temp
    else:
        temp = pow(a, (n - 1) // 2)
        return a * temp * temp
